TARGET_NO_BOOTLOADER := true
#TARGET_PROVIDES_INIT_RC := true

TARGET_ARCH := mips
TARGET_ARCH_VARIANT := mips32r2-fp-xburst
ARCH_MIPS_PAGE_SHIFT := 12
TARGET_CPU_ABI := mips
TARGET_CPU_ABI2 := mips
TARGET_BOARD_PLATFORM := M200
TARGET_BOARD_PLATFORM_GPU := GC1000
BOARD_EGL_CFG := device/ingenic/watch/config/egl.cfg
# TARGET_BOOTLOADER_BOARD_NAME MUST equals to $(ro.hardware).
TARGET_BOOTLOADER_BOARD_NAME := watch
TARGET_AAPT_CHARACTERISTICS := tablet
PRODUCT_VENDOR_KERNEL_HEADERS := hardware/ingenic/kernel-headers/3.0.8/kernel-headers

# Dalvik definition
WITH_JIT := true

# Enable dex-preoptimization to speed up the first boot sequence
# of an SDK AVD. Note that this operation only works on Linux for now
ifeq ($(HOST_OS),linux)
  ifeq ($(WITH_DEXPREOPT),)
    WITH_DEXPREOPT := true
  endif
endif

