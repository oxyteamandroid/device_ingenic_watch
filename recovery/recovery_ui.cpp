/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <linux/input.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "common.h"
#include "device.h"
#include "screen_ui.h"

static const char* HEADERS[] = { "Power key:",
                          "Short press->move",
                          "Long  press->enter",
                          "",
                          NULL };

static const char* ITEMS[] = { "reboot system now",
                        "factory reset",
                        "wipe cache",
                        NULL };

class MyDeviceUI : public ScreenRecoveryUI {
  public:
    MyDeviceUI() :
        consecutive_power_keys(0) {
    }

    virtual KeyAction CheckKey(int key) {
        if (key == KEY_HOME) {
            consecutive_power_keys = 0;
            return TOGGLE;
        }

        if (key == KEY_END) {
        //if (key == KEY_POWER) {
            ++consecutive_power_keys;
            if (consecutive_power_keys >= 7) {
                return REBOOT;
            }
        } else {
            consecutive_power_keys = 0;
        }
        return ENQUEUE;
    }

  private:
    int consecutive_power_keys;
};

class MyDevice : public Device {
  public:
    MyDevice() :
        ui(new MyDeviceUI) {
    }

    RecoveryUI* GetUI() { return ui; }

    int HandleMenuKey(int key_code, int visible) {
        if (visible) {
            switch (key_code) {
                case KEY_BACK:
                case KEY_POWER:
                        return kHighlightDown;

		case (KEY_BACK  | FLAG_KEY_LONG_PRECESS):
		case (KEY_POWER | FLAG_KEY_LONG_PRECESS):
                    return kInvokeItem;
            }
        }

        return kNoAction;
    }

    BuiltinAction InvokeMenuItem(int menu_position) {
        switch (menu_position) {
          case 0: return REBOOT;
          case 1: return WIPE_DATA;
          case 2: return WIPE_CACHE;
          default: return NO_ACTION;
        }
    }

    const char* const* GetMenuHeaders() { return HEADERS; }
    const char* const* GetMenuItems() { return ITEMS; }

  private:
    RecoveryUI* ui;
};

Device* make_device() {
    return new MyDevice;
}
