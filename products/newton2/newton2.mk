
#$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, device/ingenic/watch/products/newton2/base.mk)
# Inherit from those products. Most specific first.
$(call inherit-product, device/ingenic/watch/device.mk)

# Discard inherited values and use our own instead.
PRODUCT_NAME := newton2
PRODUCT_DEVICE := watch
PRODUCT_BRAND := Ingenic
PRODUCT_MODEL := newton2
PRODUCT_MANUFACTURER := Ingenic
PRODUCT_CHARACTERISTICS := tablet

#OTA RADIO
INSTALLED_RADIOIMAGE_TARGET += \
        $(PRODUCT_OUT)/boot.img \
        $(PRODUCT_OUT)/recovery.img \
        $(PRODUCT_OUT)/u-boot-with-spl-mbr-gpt.bin

# Kernel
ifeq ($(TARGET_PREBUILT_KERNEL),)
    LOCAL_KERNEL := device/ingenic/watch/products/newton2/kernel
else
    LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif
PRODUCT_COPY_FILES += \
    $(LOCAL_KERNEL):kernel

# Graphic definition
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := true

# Audio definition
USE_LEGACY_AUDIO_POLICY := 1
AUDIO_HAL_TYPE := alsa
#AUDIO_HAL_TYPE := oss

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/mixer_paths.xml:system/etc/mixer_paths.xml     \
    $(LOCAL_PATH)/audio_policy.conf:system/etc/audio_policy.conf

# Bluetooth definition
BOARD_HAVE_BLUETOOTH_BCM    := true
BOARD_HAVE_BLUETOOTH        := true
#BOARD_BT_MODULE             := BCM20710
#BOARD_BT_MODULE             := BCM43438
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/ingenic/watch/bluetooth
BTMAC_INTERFACE			:= efuse

# Wi-Fi hardware selection
BOARD_HAVE_WIFI             := true
BOARD_WIFI_MODULE           := BCM43438
WIFIMAC_INTERFACE		:= efuse

# Wi-Fi hardware configure
ifeq ($(BOARD_WIFI_MODULE), BCM43438)
WIFI_DRIVER_FW_PATH_STA	:= "/system/lib/firmware/wifi/fw_bcm43438.bin"
WIFI_DRIVER_FW_PATH_AP := "/system/lib/firmware/wifi/fw_bcm43438_apsta.bin"
endif
PRODUCT_DEFAULT_WIFI_CHANNELS := 13
#WIFI_DRIVER_MODULE_NAME :=  "bcmdhd"
#WIFI_DRIVER_MODULE_PATH := "/system/lib/modules/bcmdhd.ko"
BOARD_WLAN_DEVICE := bcmdhd
WPA_SUPPLICANT_VERSION := VER_0_8_X
BOARD_FLASH_BLOCK_SIZE := 512
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
#WIFI_DRIVER_FW_PATH_P2P := ""
WIFI_DRIVER_FW_PATH_PARAM := "/data/misc/wifi/wifi.conf"
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd

# Camera definition
#CAMERA_INTERFACE := misc
CAMERA_INTERFACE := v4l2
CAMERA_SUPPORT_VIDEOSNAPSHOT := false
CONVERT_WITH_SOFT := true
CAMERA_VERSION := 1
# Define facing information according to the order of sensors in 'struct ovisp_camera_client'
ID_FACING_FRONT := 0
#ID_FACING_BACK := 0

# File system definition
TARGET_USERIMAGES_USE_EXT4 := true
# system size is 600M
#BOARD_SYSTEMIMAGE_PARTITION_SIZE := 629145600
# system size is 650M
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 681574400
# data size is 960M
BOARD_USERDATAIMAGE_PARTITION_SIZE := 1006632960
# cache size is 30M
BOARD_CACHEIMAGE_PARTITION_SIZE := 31457280
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 512
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

TARGET_RECOVERY_UI_LIB := librecovery_ui_watch
TARGET_RECOVERY_PIXEL_FORMAT := "RGBX_8888"
TARGET_RECOVERY_UPDATER_LIBS += librecovery_updater_watch
TARGET_RELEASETOOLS_EXTENSIONS := device/ingenic/watch

# Invensense sensors definition
BOARD_HAL_SENSORS_USE_THIRD_PARTY := true
BOARD_HAL_SENSORS_THIRD_PARTY := invensense
BOARD_HAL_SENSORS_INVENSENSE_IIO := true
BOARD_HAL_SENSORS_AKM_COMPASS_INTEGRATED := true
BOARD_HAL_SENSORS_LIST := Gyroscope Accelerometer Magnetic Orientation

# Enable Minikin text layout engine (will be the default soon)
USE_MINIKIN := true

# # Include an expanded selection of fonts
EXTENDED_FONT_FOOTPRINT := false

#VOICE_TRIGER_SUPPORT := true

# Iwds support
BOARD_SUPPORT_IWDS := true

# debug tools
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/regrw:/system/bin/regrw
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/busybox:/system/bin/busybox

#logwatch
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/logwatch:/system/bin/logwatch
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/logwatch.conf:/system/etc/logwatch.conf

# preset local_theme resources
src_files := $(shell cd $(LOCAL_PATH)/themes/local_theme; find . ;)
PRODUCT_COPY_FILES += $(foreach file, $(src_files), \
	    $(LOCAL_PATH)/themes/local_theme/$(file):system/themes/local_theme/$(file))

# preset clock0 resources
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/clock/clock_01.png:system/clock/clock_01.png \
	$(LOCAL_PATH)/clock/widget_clock_hour_01.png:system/clock/widget_clock_hour_01.png \
	$(LOCAL_PATH)/clock/widget_clock_minute_01.png:system/clock/widget_clock_minute_01.png \
	$(LOCAL_PATH)/clock/widget_clock_second_01.png:system/clock/widget_clock_second_01.png

# preset clocks resources
src_files := $(shell cd $(LOCAL_PATH)/clock/launcher; find . ;)
PRODUCT_COPY_FILES += $(foreach file, $(src_files), \
            $(LOCAL_PATH)/clock/launcher/$(file):system/clocks/$(file))

# preset slptclocks resources
src_files := $(shell cd $(LOCAL_PATH)/clock/slpt; find . ;)
PRODUCT_COPY_FILES += $(foreach file, $(src_files), \
            $(LOCAL_PATH)/clock/slpt/$(file):system/slptclock/$(file))

#slpt app, slpt linux
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/slpt/slpt_loader.sh:system/etc/slpt_loader.sh \
    $(LOCAL_PATH)/slpt/slpt-app:system/etc/firmware/slpt-app \
    $(LOCAL_PATH)/slpt/slpt-linux:system/bin/slpt

# Additional settings used in all AOSP builds
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.android.dateformat=MM-dd-yyyy \
    ro.config.ringtone=Ring_Synth_04.ogg \
    ro.config.notification_sound=pixiedust.ogg

# Put en_US first in the list, so make it default.
PRODUCT_LOCALES := en_US zh_CN zh_TW

# Include drawables for all densities
PRODUCT_AAPT_CONFIG := normal

# Get some sounds
$(call inherit-product-if-exists, frameworks/base/data/sounds/AllAudio.mk)
# Get the TTS language packs
$(call inherit-product-if-exists, external/svox/pico/lang/all_pico_languages.mk)
# Get a list of languages.
#$(call inherit-product, $(SRC_TARGET_DIR)/product/locales_full.mk)

PRODUCT_COPY_FILES += \
    frameworks/av/media/libeffects/data/audio_effects.conf:system/etc/audio_effects.conf

PRODUCT_PROPERTY_OVERRIDES += \
    ro.carrier=wifi-only

# fonts
PRODUCT_COPY_FILES += \
    frameworks/base/data/fonts/system_fonts.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/system_fonts.xml \
    frameworks/base/data/fonts/fallback_fonts.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/fallback_fonts.xml \
    frameworks/base/data/fonts/fonts.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/fonts.xml
$(call inherit-product-if-exists, frameworks/base/data/keyboards/keyboards.mk)
$(call inherit-product-if-exists, frameworks/webview/chromium/chromium.mk)

PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.notification_sound=OnTheHunt.ogg \
    ro.config.alarm_alert=Alarm_Classic.ogg

#service hardware or soft feature,such as android.hardware.camera.xml tablet_core_hardware.xml
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/systemserver.xml:system/etc/permissions/systemserver.xml

# The order of PRODUCT_BOOT_JARS matters.
PRODUCT_BOOT_JARS := \
    core-libart \
    conscrypt \
    okhttp \
    core-junit \
    bouncycastle \
    ext \
    framework \
    voip-common \
    ims-common \
    android.policy \
    apache-xml

#   core-libart \
    conscrypt \
    okhttp \
    core-junit \
    bouncycastle \
    ext \
    framework \
    telephony-common \
    voip-common \
    ims-common \
    mms-common \
    android.policy \
    apache-xml

# The order of PRODUCT_SYSTEM_SERVER_JARS matters.
PRODUCT_SYSTEM_SERVER_JARS := \
    services \
    ethernet-service \
    wifi-service

PRODUCT_RUNTIMES := runtime_libart_default

PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.zygote=zygote32
PRODUCT_COPY_FILES += \
    system/core/rootdir/init.zygote32.rc:root/init.zygote32.rc

PRODUCT_PROPERTY_OVERRIDES +=           \
    persist.sys.timezone=Asia/Shanghai  \
    ro.sf.lcd_density=240

PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
    frameworks/base/preloaded-classes:system/etc/preloaded-classes)

# Note: it is acceptable to not have a compiled-classes file. In that case, all boot classpath
#       classes will be compiled.
PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
    frameworks/base/compiled-classes:system/etc/compiled-classes)
#product/embedded.mk
PRODUCT_COPY_FILES += \
    system/core/rootdir/init.usb.rc:root/init.usb.rc \
    system/core/rootdir/init.trace.rc:root/init.trace.rc \
    system/core/rootdir/ueventd.rc:root/ueventd.rc \
    system/core/rootdir/etc/hosts:system/etc/hosts

# bootanimation
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/bootanimation.zip:system/media/bootanimation.zip \
