#!/system/bin/sh

serial_numbers_file=/system/hw_info/serial_numbers

if [ -f "$serial_numbers_file" ]; then
    exit
fi

mount -o remount,rw /system

if [ ! -d /system/hw_info ]; then
mkdir /system/hw_info
fi

rm -rf $serial_numbers_file
touch $serial_numbers_file
# See partition distribution from
# <bootable/bootloader/uboot/board/ingenic/watch/partitions.tab>
dd  if=/dev/block/platform/jzmmc_v1.2.0/by-name/misc of=$serial_numbers_file bs=128 count=1

chmod 0644 $serial_numbers_file

mount -o remount,ro /system
