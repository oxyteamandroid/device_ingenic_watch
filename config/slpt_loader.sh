#!/system/bin/sh

/system/bin/mount -o remount,rw /system
cp sbin/script/slpt-app /etc/firmware/slpt-app
/system/bin/mount -o remount,ro /system

# wait a little while
sleep 2
# load slpt app
echo slpt-app > /sys/slpt/add
echo slpt-app > /sys/slpt/task
echo 1 > /sys/slpt/enable

sleep 2
echo -e "\x3c\x00\x00\x00" > /sys/slpt/apps/slpt-app/res/sample-period/data
