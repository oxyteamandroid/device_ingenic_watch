# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Emit extra commands needed for Group during OTA installation
(installing the bootloader)."""

import common

def FullOTA_InstallEnd(info):
  try:
    bootloader_bin = info.input_zip.read("RADIO/u-boot-with-spl-mbr-gpt.bin")
  except KeyError:
    print "no u-boot-with-spl-mbr-gpt.bin in target_files; skipping install"
  else:
    WriteBootloader(info, bootloader_bin)


def WriteBootloader(info, bootloader_bin):
  print "Writing u-boot-with-spl-mbr-gpt.bin into target zip"
  common.ZipWriteStr(info.output_zip, "u-boot-with-spl-mbr-gpt.bin", bootloader_bin)

